const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const mongoose = require('mongoose')
const config = require('./config/config')

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('combined'))
app.use(cors())

mongoose.connect(config.db.url, {useMongoClient: true})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))

app.get('/', function (request, response) {
	response.send('hi')
})

app.post('/products', (req, res) => {
	db.collection('products').save(req.body, (err, result) => {
		if (err) return console.log(err)
		res.send('saved to database ' + result)
	})
})

app.listen(config.port)
console.log(`Server started on port ${config.port}`)
