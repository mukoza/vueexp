export const LAST_SYNC = 'lastSynchronization'
export const API_URL = 'http://185.51.247.73/foxlist'
export const BEGINNING_OF_TIME = '1970-01-01-01-01-01'

export const ORDERS_ROUTER_MAPPING = {
  NEW: 'Order',
  PROCESSING: 'OrderProcessing'
}
