import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login/Login.vue'
import Register from '@/components/register/Register.vue'
import Orders from '@/components/orders/Orders.vue'
import Order from '@/components/order/Order.vue'
import OrderProcessing from '@/components/orderProcessing/OrderProcessing.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Orders
    }, {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/register',
      name: 'Register',
      component: Register
    }, {
      path: '/orders',
      name: 'Orders',
      component: Orders
    }, {
      path: '/orders/:orderId',
      name: 'Order',
      component: Order
    }, {
      path: '/orders-processing/:orderId',
      name: 'OrderProcessing',
      component: OrderProcessing
    }

  ]
})
