import axios from 'axios'
import store from '@/store/store'
import * as constants from '@/constants'

export default() => {
  return axios.create({
    baseURL: constants.API_URL,
    headers: {
      'token': `${store.state.token}`
    }
  })
}
