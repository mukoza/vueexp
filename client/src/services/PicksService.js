import Api from '@/services/Api'
import * as constants from '@/constants'

export default {
  getPicks(orderId) {
    return Api().get(`picks/${orderId}/${constants.BEGINNING_OF_TIME}`)
  },
  addPick(orderId, productId) {
    let picks = {
      orders: {
        remoteId: orderId
      },
      product: {
        remoteId: productId
      }
    }
    return Api().post('picks', picks)
  },
  changePickAmount(pick) {
    Api().patch(`picks/${pick.remoteId}`, {amount: pick.amount})
    return pick.amount
  },
  changePickStatus(pick) {
    return Api().patch(`picks/${pick.remoteId}`, {status: pick.status})
  },
  removePick(pick) {
    return Api().delete(`picks/${pick.remoteId}`)
  }
}
