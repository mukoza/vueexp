import Api from '@/services/Api'
import * as constants from '@/constants'

export default {
  getProducts() {
    return Api().get(`products/${constants.BEGINNING_OF_TIME}`)
  }
}
