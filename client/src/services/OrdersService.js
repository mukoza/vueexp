import Api from '@/services/Api'
import * as constants from '@/constants'

export default {
  getOrders() {
    return Api().get('orders', {
      headers: {
        [constants.LAST_SYNC]: constants.BEGINNING_OF_TIME
      }
    })
  },
  get(orderId) {
    return Api().get(`orders/${orderId}`)
  },
  createOrder() {
    const options = {
      hour12: false
    }
    return Api().post('orders', {
      name: new Date().toLocaleString('ru-Ru', options)
    })
  },
  deleteOrder(orderId) {
    return Api().delete(`orders/${orderId}`)
  },
  changeOrderStatus(orderId, newStatus) {
    return Api().patch(`orders/${orderId}`, {status: newStatus})
  }
}
