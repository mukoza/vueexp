import Api from '@/services/Api'

export default {
  register(credentials) {
    return Api().post('users/', credentials)
  },
  login(credentials) {
    return Api().get('users/'.concat(credentials.login).concat('/').concat(credentials.password))
  }
}
