export default {
  methods: {
    logout() {
      this
        .$store
        .dispatch('setToken', null)
      this
        .$router
        .push({name: 'Home'})
    }
  }
}
