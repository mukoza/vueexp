import AuthenticationService from '@/services/AuthenticationService'

export default {
  data() {
    return {
      login: '',
      password: '',
      valid: false,
      passwordRules: [
        (v) => !!v || 'Password is required',
        (v) => v.length > 3 || 'Password should be minimum 4 chars'
      ],
      loginRules: [
        (v) => !!v || 'Login is required',
        (v) => v.length > 3 || 'Login should be minimum 4 chars'
      ]
    }
  },
  methods: {
    async signup() {
      try {
        const response = await AuthenticationService.register({
          login: this.login,
          password: this.password
        })
        this.$store.dispatch('setToken', response.data.token)
        this.$router.push({
          name: 'Orders'
        })
      } catch (error) {
        console.error(error)
      }
    }
  }
}
