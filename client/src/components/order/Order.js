import { mapState } from 'vuex'
import ProductsService from '@/services/ProductsService'
import OrdersService from '@/services/OrdersService'
import PicksService from '@/services/PicksService'
import Product from '@/components/product/Product'
import PickedProduct from '@/components/productPicked/PickedProduct'

import * as filter from '@/components/order/FilterLogic'
var _ = require('lodash')

export default {
  data () {
    return {
      order: {},
      productsList: [],
      products: [],
      picks: [],
      productFilter: '',
      groupFilter: '',
      groups: [],
      showProgress: true
    }
  },
  computed: {
    ...mapState([
      'isUserLoggedIn',
      'route'
    ]),
    binding () {
      const binding = {}
      binding.column = !this.$vuetify.breakpoint.mdAndUp
      return binding
    }
  },
  components: {
    Product, PickedProduct
  },
  beforeCreate () {
    if (!this.$store.state.isUserLoggedIn) {
      this.$router.push({
        name: 'Home'
      })
    }
  },
  async mounted () {
    const orderId = this.$store.state.route.params.orderId
    try {
      this.order = (await OrdersService.get(orderId)).data
      this.productsList = (await ProductsService.getProducts()).data
        .sort((a, b) => a.name.localeCompare(b.name))
      this.groups = _.uniq(this.productsList
        .map(product => product.groups.name))
        .sort((a, b) => a.localeCompare(b))
      this.picks = (await PicksService.getPicks(orderId)).data
        .sort((a, b) => a.product.name.localeCompare(b.product.name))
      this.products = filter.applyFullFilter(this)
      this.showProgress = false
    } catch (error) {
      console.log(error)
    }
  },
  methods: {
    update (pick) {
      this.products = this.products.filter(product => product.remoteId !== pick.product.remoteId)
      this.picks.push(pick)
      this.picks.sort((a, b) => a.product.name.localeCompare(b.product.name))
    },
    remove (pick) {
      this.products.push(pick.product)
      this.products.sort((a, b) => a.name.localeCompare(b.name))
      this.picks = this.picks.filter(it => it.remoteId !== pick.remoteId)
    },
    async completeOrder () {
      await OrdersService.changeOrderStatus(this.order.remoteId, 'PROCESSING')
      this.$router.push({
        name: 'Orders'
      })
    },
    nameFilterChange() {
      this.products = filter.applyFullFilter(this)
    },
    groupFilterSelect(selectedGroupFilter, handler) {
      this.groupFilter = selectedGroupFilter
      this.products = filter.applyFullFilter(this)
    }

  }

}
