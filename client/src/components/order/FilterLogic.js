function getProducts(order) {
  const productsInPicks = order.picks.map(item => item.product.remoteId)
  return order.productsList.filter(product => !productsInPicks.includes(product.remoteId))
}

export function applyFullFilter(order) {
  let products = getProducts(order)
  if (order.productFilter !== '') {
    products = products.filter(product => product.name.toLowerCase().includes(order.productFilter))
  }
  if (order.groupFilter) {
    products = products.filter(product => product.groups.name === order.groupFilter)
  }
  return products
}
