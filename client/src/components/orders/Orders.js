import OrdersService from '@/services/OrdersService'
import * as constants from '@/constants'

export default {
  data () {
    return {
      orders: []
    }
  },
  async mounted () {
    try {
      this.orders = (await OrdersService.getOrders()).data
    } catch (error) {
      console.log(error)
    }
  },
  created () {
    this.apiUrl = constants.API_URL
    this.mapping = constants.ORDERS_ROUTER_MAPPING
  },
  methods: {
    async create () {
      const createdOrder = (await OrdersService.createOrder()).data
      this.orders.push(createdOrder)
    },
    async deleteOrder (orderId) {
      await OrdersService.deleteOrder(orderId)
      this.orders = this.orders.filter(it => it.remoteId !== orderId)
    },
    orderIcon (order) {
      return 'order_' + order.status
    }
  }
}
