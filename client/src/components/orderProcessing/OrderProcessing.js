import {mapState} from 'vuex'
import ProductsService from '@/services/ProductsService'
import OrdersService from '@/services/OrdersService'
import PicksService from '@/services/PicksService'
import ProductToBuy from '@/components/productToBuy/ProductToBuy'

export default {
  data() {
    return {order: {}, products: [], picks: []}
  },
  computed: {
    ...mapState(['isUserLoggedIn', 'route']),
    binding() {
      const binding = {}
      if (this.$vuetify.breakpoint.mdAndUp) {
        binding.column = true
      }
      return binding
    }
  },
  components: {
    ProductToBuy
  },
  beforeCreate() {
    if (!this.$store.state.isUserLoggedIn) {
      this
        .$router
        .push({name: 'Home'})
    }
  },
  async mounted() {
    const orderId = this.$store.state.route.params.orderId
    try {
      this.order = (await OrdersService.get(orderId)).data
      this.products = (await ProductsService.getProducts())
        .data
        .sort((a, b) => a.name.localeCompare(b.name))
      this.picks = (await PicksService.getPicks(orderId))
        .data
        .sort((a, b) => a.product.name.localeCompare(b.product.name))
      const productsInPicks = this
        .picks
        .map(item => item.product.remoteId)
      this.products = this
        .products
        .filter(product => !productsInPicks.includes(product.remoteId))
    } catch (error) {}
  },
  methods: {
    async buyOrReturnPick(pickRemoteId) {
      const foundPick = this.picks
        .find(pick => pick.remoteId === pickRemoteId)
      foundPick.status = foundPick.status === 'PICKED'
        ? 'BOUGHT'
        : 'PICKED'
      await PicksService.changePickStatus(foundPick)
      if (!this.picks.some(pick => pick.status === 'PICKED')) {
        this.$router
          .push({name: 'Orders'})
      }
    }
  }

}
